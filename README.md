# Edinburgh Wolves app for Android

An unofficial Android app for the British American Football Association National League team, the _Edinburgh Wolves_!

## Current Features:

- Displays the most recent opponent and score
- Displays current table standings
